from setuptools import setup, find_packages
import os
import re
import io

# Read the long description from the readme file
with open("README.md", "rb") as f:
    long_description = f.read().decode("utf-8")

# Run setup
setup(name='ml_atmosphere',
      packages=find_packages(),
      description='Package for atmospheric condition estimation.',
      long_description=long_description,
      author='George Doxastakis',
      author_email='gdoxastakis@raymetrics.com',
      keywords='lidar aerosol temperature hsrl',
      install_requires=[
          "numpy",
          "matplotlib",
          "pandas",
          "sklearn",
          "tensorflow"
      ]
      ,
      entry_points={
         'console_scripts': ['ml_method_comparison = ml_atmosphere.scripts.ml_method_comparison:main'],
      }
      )
