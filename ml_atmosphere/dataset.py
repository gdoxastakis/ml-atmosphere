import pandas as pd
import re


def load_from_csv(filename, test_idx=8768):
    df = pd.read_csv(filename, index_col=0)
    target_cols = [col for col in list(df.columns) if re.match("^w[0-9]+$", col)]
    target_df = df[target_cols]
    input_df = df[[col for col in list(df.columns) if col not in target_cols]]

    input_train_df = input_df.iloc[:test_idx]
    input_test_df = input_df.iloc[test_idx:]
    target_train_df = target_df.iloc[:test_idx]
    target_test_df = target_df.iloc[test_idx:]
    return input_train_df, input_test_df, target_train_df, target_test_df
