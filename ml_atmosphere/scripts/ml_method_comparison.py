from ml_atmosphere.dataset import load_from_csv
import argparse
from sklearn import linear_model
from sklearn.metrics import r2_score, mean_absolute_error
from sklearn.preprocessing import PolynomialFeatures, MinMaxScaler
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow import keras


def main():
    # Define the command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dataset_path', help='Path to a dataset csv file.')
    args = parser.parse_args()

    inputs_train, inputs_test, targets_train, targets_test = load_from_csv(args.dataset_path)

    scaler = MinMaxScaler()
    scaler.fit(targets_train)
    targets_train_scaled = scaler.transform(targets_train)
    targets_test_scaled = scaler.transform(targets_test)

    # Polynomial
    order = 4
    print("Polynomial and interaction features order {} with linear regression.".format(order))
    poly = PolynomialFeatures(degree=order)
    inputs_train_poly = poly.fit_transform(inputs_train)
    inputs_test_poly = poly.fit_transform(inputs_test)
    poly_names = poly.get_feature_names(input_features=inputs_train.columns)

    reg = linear_model.LinearRegression(fit_intercept=False)
    reg.fit(inputs_train_poly, targets_train_scaled)
    test_pred_scaled = reg.predict(inputs_test_poly)
    print('Test mae {}'.format(mean_absolute_error(targets_test_scaled, test_pred_scaled)))
    print('Test r2 score {}'.format(r2_score(targets_test_scaled, test_pred_scaled,
                                             multioutput='variance_weighted')))
    df = pd.DataFrame(reg.coef_, columns=poly_names, index=list(targets_train.columns))
    filename = "poly_fit_coef.csv"
    df.to_csv(filename)
    print("Coefficients saved to '{}'".format(filename))

    # NN
    x_train = np.nan_to_num(np.asarray(inputs_train)).astype('float64')
    y_train = np.nan_to_num(np.asarray(targets_train_scaled)).astype('float64')
    x_test = np.nan_to_num(np.asarray(inputs_test)).astype('float64')
    y_test = np.nan_to_num(np.asarray(targets_test_scaled)).astype('float64')
    with tf.device("/cpu:0"):
        model = keras.Sequential()
        model.add(keras.layers.Dense(units=128, activation='tanh', input_dim=x_train.shape[1]))
        model.add(keras.layers.Dense(units=y_train.shape[1], activation='linear'))

        model.compile(optimizer="Adam", loss="mean_absolute_error")

        model.fit(x_train, y_train, epochs=100, verbose=2)
        test_pred_scaled = model.predict(x_test)

        print('Test mae {}'.format(mean_absolute_error(targets_test_scaled, test_pred_scaled)))
        print('Test r2 score {}'.format(r2_score(targets_test_scaled, test_pred_scaled,
                                                 multioutput='variance_weighted')))


if __name__ == "__main__":
    main()
